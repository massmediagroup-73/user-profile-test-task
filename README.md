# User-profile-test-task

## Description
This project represents front-end part of a User Profile. This front-end development based on Vue js (typescript), JSON server, Vue Test Utils.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Run your unit tests
```
yarn run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Fake API (JSON server)
Install json-server:
```
yarn add json-server
```
Go to src and run:
```
node server.ts
```
Now all data will be available throught http://localhost:3000/ (example http://localhost:3000/users)
