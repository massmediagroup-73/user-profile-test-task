import Users from '@/components/Users.vue'
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils'
import Vue from 'vue'
import Vuex from 'vuex'
import usersListFixture from './fixtures/users'

const localVue = createLocalVue()
localVue.use(Vuex)

let wrapper: Wrapper<Vue>
let store
let getters

beforeEach(() => {
  getters = {
    userId: jest.fn(),
  }
  store = new Vuex.Store({
    modules: {
      auth: {
        namespaced: true,
        getters,
      },
    },
  })
  wrapper = shallowMount(Users, {
    propsData: {
      users: [],
    },
    store,
    localVue,
  })
})

afterEach(() => {
  wrapper.destroy()
})

describe('Users', () => {
  it('is a Vue instance', () => {
    expect(wrapper.isVueInstance).toBeTruthy()
  })
  it('shows empty users text', () => {
    expect(wrapper.contains('.users__list')).toBe(false)
    expect(wrapper.text()).toMatch('No users')
  })
  it('get users list as props', () => {
    wrapper.setProps({ users: usersListFixture })

    expect(wrapper.props().users).toBe(usersListFixture)
  })
  it('renders users list', () => {
    wrapper.setProps({ users: usersListFixture })

    expect(wrapper.contains('.users__list')).toBe(true)
  })
  it('renders user name', () => {
    wrapper.setProps({ users: usersListFixture })

    expect(wrapper.find('.users__list-item:first-of-type').text()).toBe(
      wrapper.props().users[0].screenName
    )
  })
})
