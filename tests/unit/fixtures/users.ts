export default [
  {
    id: 1,
    screenName: 'Sam Guilty',
    avatar: 'https://api.adorable.io/avatars/285/sam@adorable.io.png',
    followers: 0,
  },
  {
    id: 2,
    screenName: 'Sandra Toe',
    avatar: 'https://api.adorable.io/avatars/285/sandra@adorable.io.png',
    followers: 0,
  },
  {
    id: 3,
    screenName: 'Monika July',
    avatar: 'https://api.adorable.io/avatars/285/monika@adorable.io.png',
    followers: 0,
  },
]
