import Profile from '@/components/Profile.vue'
import { shallowMount, Wrapper } from '@vue/test-utils'
import Vue from 'vue'
import usersListFixture from './fixtures/users'
import followingsFixture from './fixtures/followings'
import { UserProfile } from '@/types'
import mockAxios from 'axios'
import flushPromises from 'flush-promises'

const viewerId: number = usersListFixture[0].id
const userProfile: UserProfile = {
  ...usersListFixture[1],
  followedByMe: false,
}
;(mockAxios.get as jest.Mock).mockImplementation(url => {
  if (url === 'followings') {
    return Promise.resolve({
      data: followingsFixture,
    })
  } else if (url === `users/${userProfile.id}`) {
    return Promise.resolve({
      data: userProfile,
    })
  }
})
;(mockAxios.post as jest.Mock).mockImplementation(url => {
  if (url === 'followings') {
    return Promise.resolve({
      data: {
        userId: userProfile.id,
        followerId: viewerId,
      },
    })
  } else if (url === `followings/${userProfile.id}`) {
    return Promise.resolve({
      data: {
        userId: userProfile.id,
        followerId: viewerId,
      },
    })
  }
})
;(mockAxios.patch as jest.Mock).mockImplementation(url => {
  if (url === `users/${userProfile.id}`) {
    return Promise.resolve({
      data: {
        userProfile,
      },
    })
  }
})

let wrapper: Wrapper<Vue>

beforeEach(() => {
  jest.resetModules()
  jest.clearAllMocks()
  wrapper = shallowMount(Profile, {
    propsData: {
      user: userProfile,
      viewerId: viewerId,
    },
  })
})

afterEach(() => {
  wrapper.destroy()
})

describe('Profile', () => {
  it('is a Vue instance', () => {
    expect(wrapper.isVueInstance).toBeTruthy()
  })
  it('has render container', () => {
    expect(wrapper.contains('.profile')).toBeTruthy()
  })
  it('has render user data', () => {
    expect(wrapper.find('.profile__name').text()).toMatch(
      userProfile.screenName
    )
    expect(wrapper.find('.profile__followers').text()).toMatch(
      userProfile.followers.toString()
    )
    expect(wrapper.find('.profile__action').text()).toMatch('Follow')
  })
  it('follow user', async done => {
    const followButton = wrapper.find('.profile__action button')
    followButton.trigger('click')

    await flushPromises()

    expect(wrapper.emitted().updateUser).toBeTruthy()
    wrapper.setProps({
      user: {
        ...userProfile,
        followedByMe: true,
        followers: 1,
      },
    })
    expect(wrapper.find('.profile__followers').text()).toMatch('1')
    expect(wrapper.find('.profile__action').text()).toMatch('Unfollow')
    done()
  })
  it('unfollow user', async done => {
    wrapper.setProps({
      user: {
        ...userProfile,
        followedByMe: true,
        followers: 1,
      },
    })
    expect(wrapper.find('.profile__action').text()).toMatch('Unfollow')
    const followButton = wrapper.find('.profile__action button')
    followButton.trigger('click')

    await flushPromises()

    wrapper.setProps({
      user: {
        ...userProfile,
        followedByMe: false,
        followers: 0,
      },
    })
    expect(wrapper.emitted().updateUser).toBeTruthy()
    expect(wrapper.find('.profile__followers').text()).toMatch('0')
    expect(wrapper.find('.profile__action').text()).toMatch('Follow')
    done()
  })
})
