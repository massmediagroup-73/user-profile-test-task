import ProfilePage from '@/views/ProfilePage.vue'
import Profile from '@/components/Profile.vue'
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils'
import Vue from 'vue'
import Vuex from 'vuex'
import usersListFixture from './fixtures/users'
import followingsFixture from './fixtures/followings'
import mockAxios from 'axios'
import flushPromises from 'flush-promises'
import { UserProfile } from '@/types'

const viewerId: number = usersListFixture[0].id
const userProfile: UserProfile = usersListFixture[1]
;(mockAxios.get as jest.Mock).mockImplementation(url => {
  if (url === 'followings') {
    return Promise.resolve({
      data: followingsFixture,
    })
  } else if (url === `users/${userProfile.id}`) {
    return Promise.resolve({
      data: userProfile,
    })
  }
})

const $route = {
  path: `/users/${userProfile.id}/profile?viewer=${viewerId}`,
  hash: '',
  params: {
    id: userProfile.id,
  },
  query: {
    viewer: viewerId,
  },
}

const localVue = createLocalVue()
localVue.use(Vuex)

let wrapper: Wrapper<Vue>
let store
let getters

beforeEach(() => {
  jest.resetModules()
  jest.clearAllMocks()
  getters = {
    userId: jest.fn(() => viewerId),
  }
  store = new Vuex.Store({
    modules: {
      auth: {
        namespaced: true,
        getters,
      },
    },
  })
  wrapper = shallowMount(ProfilePage, {
    store,
    mocks: {
      $route,
    },
    localVue,
  })
})

afterEach(() => {
  wrapper.destroy()
})

describe('ProfilePage', () => {
  it('is a Vue instance', () => {
    expect(wrapper.isVueInstance).toBeTruthy()
  })
  it('has render container', () => {
    expect(wrapper.contains('.profile-page')).toBeTruthy()
  })
  it('renders user profile not found', () => {
    expect(wrapper.contains('.profile-page__not-found')).toBe(true)
    expect(wrapper.text()).toMatch(':( User does not exist')
  })
  it('renders profile component', async done => {
    await flushPromises()

    expect(wrapper.contains('.profile-page__not-found')).toBe(false)
    expect(wrapper.find(Profile).exists()).toBe(true)
    expect(wrapper.find(Profile).vm.$props.user).toEqual(userProfile)
    expect(wrapper.find(Profile).vm.$props.viewerId).toEqual(viewerId)
    done()
  })
})
