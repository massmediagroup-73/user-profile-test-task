import App from '@/App.vue'
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils'
import Vue from 'vue'
import Vuex, { ActionTree } from 'vuex'
import { AuthState } from '@/store/auth/types'
import { RootState } from '@/store/types'
import VueProgressBar from 'vue-progressbar'
import PageTransition from '@/components/global/transition/PageTransition.vue'
import usersListFixture from './fixtures/users'
import { User } from '@/types'
import mockAxios from 'axios'
import VueRouter from 'vue-router'

const user: User = usersListFixture[0]
;(mockAxios.get as jest.Mock).mockImplementation(url => {
  if (url === `users/${user.id}`) {
    return Promise.resolve({
      data: user,
    })
  }
})

const router = new VueRouter()
const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueProgressBar)
localVue.use(VueRouter)

let wrapper: Wrapper<Vue>
let store
let state
let actions: ActionTree<AuthState, RootState>
let mutations

beforeEach(() => {
  actions = {
    getInitialUser: jest.fn(),
  }
  mutations = {
    setUser: jest.fn(),
  }
  state = {
    user: undefined,
  }
  store = new Vuex.Store({
    modules: {
      auth: {
        namespaced: true,
        actions,
        mutations,
        state,
      },
    },
  })
  wrapper = shallowMount(App, {
    stubs: {
      PageTransition,
    },
    store,
    localVue,
    router,
  })
})

afterEach(() => {
  wrapper.destroy()
})

describe('App', () => {
  it('is a Vue instance', () => {
    expect(wrapper.isVueInstance).toBeTruthy()
  })
  it('has set initial user', () => {
    expect(actions.getInitialUser).toHaveBeenCalled()
  })
})
