import Home from '@/views/Home.vue'
import Users from '@/components/Users.vue'
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils'
import Vue from 'vue'
import Vuex from 'vuex'
import usersListFixture from './fixtures/users'
import mockAxios from 'axios'
;(mockAxios.get as jest.Mock).mockImplementation(url => {
  if (url === 'users') {
    return Promise.resolve({
      data: usersListFixture,
    })
  }
})

const localVue = createLocalVue()
localVue.use(Vuex)

let wrapper: Wrapper<Vue>
let store
let getters

beforeEach(() => {
  jest.resetModules()
  jest.clearAllMocks()
  getters = {
    userId: jest.fn(),
  }
  store = new Vuex.Store({
    modules: {
      auth: {
        namespaced: true,
        getters,
      },
    },
  })
  wrapper = shallowMount(Home, {
    store,
    localVue,
  })
})

afterEach(() => {
  wrapper.destroy()
})

describe('Home', () => {
  it('is a Vue instance', () => {
    expect(wrapper.isVueInstance).toBeTruthy()
  })
  it('has render container', () => {
    expect(wrapper.contains('.home')).toBeTruthy()
  })
  it('has render users list', () => {
    expect(wrapper.find(Users).exists()).toBe(true)
  })
  it('passes users array as a prop to Users component', () => {
    expect(wrapper.vm.$data.users).toEqual(usersListFixture)
    expect(wrapper.find(Users).vm.$props.users).toEqual(usersListFixture)
  })
})
