import { AxiosResponse } from 'axios'
export { AxiosResponse }

export interface User {
  id: number
  screenName: string
  avatar: string
  followers: number
}

export interface UserProfile {
  id: number
  screenName: string
  avatar: string
  followers: number
  followedByMe?: Boolean
}

export interface ProfileUrlQuery {
  viewer?: string
}

export interface Followings {
  id?: number
  userId: number
  followerId: number
}
