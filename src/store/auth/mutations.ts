import { MutationTree } from 'vuex'
import { AuthState } from './types'
import { User } from '@/types'

export const mutations: MutationTree<AuthState> = {
  setUser(state, payload: User) {
    state.user = payload
  },
}
