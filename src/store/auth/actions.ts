import { ActionTree } from 'vuex'
import { AuthState } from './types'
import { RootState } from '../types'
import { AxiosResponse, User } from '@/types'
import userService from '@/services/userService'

export const actions: ActionTree<AuthState, RootState> = {
  async getInitialUser({ commit }, id) {
    const response: AxiosResponse = await userService.getUser(id)
    const user: User = response.data

    commit('setUser', user)
  },
}
