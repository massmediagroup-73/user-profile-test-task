import Api from '@/services/api'
import { AxiosResponse, Followings, UserProfile } from '@/types'
import followService from './followService'

export default {
  getUser(id: number) {
    return Api().get(`users/${id}`)
  },
  getAllUsers() {
    return Api().get('users')
  },
  async getUserProfile(id: number, viewerId: number) {
    const userResponse: AxiosResponse = await this.getUser(id)
    const user: UserProfile = userResponse.data

    const followingsResponse: AxiosResponse = await followService.getFollowings()
    const followings: Followings[] = followingsResponse.data

    if (followings.length) {
      const isFollowing: Followings | undefined = followings.find(
        followRelationship =>
          followRelationship.userId === user.id &&
          followRelationship.followerId === viewerId
      )

      user.followedByMe = !!isFollowing
    } else {
      user.followedByMe = false
    }

    return user
  },
}
