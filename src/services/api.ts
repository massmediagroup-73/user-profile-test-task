import axios from 'axios'
import errorHandler from './errorHandler'

export default () => {
  const axiosInstance = axios.create({
    baseURL: process.env.VUE_APP_API_BASE_URL,
  })

  axiosInstance.interceptors.response.use(
    request => request,
    error => errorHandler(error)
  )

  return axiosInstance
}
