import Api from '@/services/api'
import { AxiosResponse, Followings, UserProfile } from '@/types'
import userService from './userService'

export default {
  getFollowings() {
    return Api().get('followings')
  },
  async setFollow(id: number, viewerId: number) {
    const followRelationData: Followings = {
      userId: id,
      followerId: viewerId,
    }

    const followingsResponse: AxiosResponse = await this.getFollowings()
    const followings: Followings[] = followingsResponse.data

    if (followings.length) {
      const isFollowingRelationship: Followings | undefined = followings.find(
        followRelationship =>
          followRelationship.userId === followRelationData.userId &&
          followRelationship.followerId === followRelationData.followerId
      )
      if (isFollowingRelationship) {
        const response: AxiosResponse = await this.unFollow(
          isFollowingRelationship
        )
        await this.toggleFollower(followRelationData.userId, false)

        return response
      } else {
        const response: AxiosResponse = await this.follow(followRelationData)
        await this.toggleFollower(followRelationData.userId, true)

        return response
      }
    } else {
      const response: AxiosResponse = await this.follow(followRelationData)
      await this.toggleFollower(followRelationData.userId, true)

      return response
    }
  },
  follow(followRelationData: Followings) {
    return Api().post('followings', followRelationData)
  },
  unFollow(isFollowingRelationship: Followings) {
    return Api().delete(`followings/${isFollowingRelationship.id}`)
  },
  async toggleFollower(id: number, state: boolean) {
    const userResponse: AxiosResponse = await userService.getUser(id)
    const user: UserProfile = userResponse.data

    return Api().patch(`users/${id}`, {
      followers: state
        ? user.followers + 1
        : user.followers !== 0
        ? user.followers - 1
        : 0,
    })
  },
}
