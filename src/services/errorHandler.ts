import { AxiosError } from 'axios'

export default (error: AxiosError) => {
  // TODO:: Handle errors

  return Promise.reject({ ...error })
}
